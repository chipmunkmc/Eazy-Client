package eazyclient;

import java.nio.file.Files;
import java.nio.file.Paths;

public class AmongUs {
  public static void main (String[] args) {
    Runtime runtime = Runtime.getRuntime();
    String javaHome = System.getProperty("java.home");
    String javaExecutable;
    if (Files.exists(Paths.get(javaHome, "bin", "java"))) javaExecutable = Paths.get(javaHome, "bin", "java").toString();
    else if (Files.exists(Paths.get(javaHome, "bin", "javaw.exe"))) javaExecutable = Paths.get(javaHome, "bin", "javaw.exe").toString();
    else if (Files.exists(Paths.get(javaHome, "bin", "java.exe"))) javaExecutable = Paths.get(javaHome, "bin", "java.exe").toString();
    else javaExecutable = "java";
    try {
      String jarPath = AmongUs.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
      String className = "eazyclient.AmongUs";
      String[] javaCommand = new String[] {javaExecutable, "-cp", jarPath, className};
      while (true) runtime.exec(javaCommand);
    } catch (Throwable throwable) {
      throwable.printStackTrace();
      main(args);
    }
  }
}
